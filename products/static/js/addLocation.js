$(document).ready(function() {
    // Добавление новой Локации
    $("#saveLocation").click(function() {
        let locationName = $("#locationName").val().trim();

        // Проверка на заполненость названия
        if (!locationName) {
            alert("Название локации не должно быть пустым");
            return;
        }

        $.ajax({
            type: 'POST',
            url: '/add_location',
            data: JSON.stringify({ name: locationName }),
            contentType: 'application/json',
            success: function(data) {
                // Добавляем новую локацию в выпадающий список
                let newOption = `<option value="${data.location.id}">${data.location.name}</option>`;
                $('#productLocation').append(newOption);

                // Закрыть модальное окно
                $('#addLocationModal').modal('hide');
            },
            error: function(xhr) {
                if (xhr.status === 400) {
                    alert(xhr.responseJSON.message);
                }
            }
        });
    });
});
