$(document).ready(function() {
    // Функция для получения текущего фильтра локаций
    function getCurrentLocationsFilter() {
        return $('#filterForm .form-check-input:checked').map(function() {
            return this.value;
        }).get().join(',');
    }

    // Функция для очистки фильтра
    $('#clearFilter').click(function() {
        $('#filterForm .form-check-input').prop('checked', false);
        $('#priceMin').val('');
        $('#priceMax').val('');
        $('#quantityMin').val('');
        $('#quantityMax').val('');
        // Автоматический поиск после очистки фильтра
        $('#searchFilter').click();
    });

    // Функция для фильтрации товаров
    $('#searchFilter').click(function() {
        let selectedLocations = getCurrentLocationsFilter();
        let priceMin = $('#priceMin').val();
        let priceMax = $('#priceMax').val();
        let searchQuery = $('#searchInput').val().trim();
        let quantityMin = $('#quantityMin').val();
        let quantityMax = $('#quantityMax').val();

        $.get('/products', {
            locations: selectedLocations,
            price_min: priceMin,
            price_max: priceMax,
            search_query: searchQuery,
            quantity_min: quantityMin,
            quantity_max: quantityMax
        }, function(data) {
            $('#tableContainer').html(data.table);
            $('#filterModal').modal('hide');
        });
    });
});
