$(document).ready(function() {
    // Обработка нажатия на кнопку "Добавить на склад"
    $(document).on('click', '.add-to-inventory', function() {
        let inventoryId = $(this).data('inventory-id');
        let quantityChange = 1;
        updateQuantity(inventoryId, quantityChange);
    });

    // Обработка нажатия на кнопку "Удалить со склада"
    $(document).on('click', '.remove-from-inventory', function() {
        let inventoryId = $(this).data('inventory-id');
        let quantityChange = -1;
        updateQuantity(inventoryId, quantityChange);
    });

    // Обновление значения количества товара на странице и в БД
    function updateQuantity(inventoryId, quantityChange) {
        $.ajax({
            type: 'POST',
            url: '/add_to_quantity',
            data: JSON.stringify({ inventory_id: inventoryId, quantity_change: quantityChange }),
            contentType: 'application/json',
            success: function(data) {
                if (data.hasOwnProperty('message')) {
                    alert(data.message);
                }
                else {
                    let quantityId = '#quantity-' + inventoryId;
                    $(quantityId).text(data.new_quantity);
                }
            }
        });
    }
});