$(document).ready(function() {
    // Функция для получения текущего фильтра локаций
    function getCurrentLocationsFilter() {
        return $('#filterForm .form-check-input:checked').map(function() {
            return parseInt(this.value, 10);
        }).get();
    }

    // Добавление нового продукта
    $('#saveProduct').click(function() {
        let name = $('#productName').val().trim();
        let description = $('#productDescription').val();
        let priceString  = $('#productPrice').val();
        let locationId = $('#productLocation').val();
        let quantityString = $('#productQuantity').val();
        let priceMin = $('#priceMin').val();
        let priceMax = $('#priceMax').val();
        let quantityMin = $('#quantityMin').val();
        let quantityMax = $('#quantityMax').val();

        // Преобразование значений цены и количества товара на складе
        let price = priceString ? parseFloat(parseFloat(priceString).toFixed(2)) : 0;
        let quantity = quantityString ? parseInt(quantityString, 10) : 0;

        // Проверки заполненности обязательных полей
        if (!name) {
            alert("Название товара не должно быть пустым");
            return;
        }
        if (!locationId) {
            alert("Локация должна быть выбрана");
            return;
        }

        let searchQuery = $('#searchInput').val().trim();
        let selectedLocations = getCurrentLocationsFilter();
        $.ajax({
            type: 'POST',
            url: '/add_product',
            data: JSON.stringify({
                name: name,
                description: description,
                price: price,
                location_id: locationId,
                quantity: quantity,
                search_query: searchQuery,
                selected_locations: selectedLocations,
                price_min: priceMin,
                price_max: priceMax,
                quantity_min: quantityMin,
                quantity_max: quantityMax
            }),
            contentType: 'application/json',
            success: function(data) {
                // Добавляем новую строку в таблицу товаров, если она соответствует критериям фильтрации
                $('table tbody').append(data.rendered_row);

                // Вывод сообщения о созданном товаре, которые не соответствует фильтрам
                if (data.hasOwnProperty('message')) {
                    alert(data.message);
                }

                // Закрыть модальное окно
                $('#addProductModal').modal('hide');
            },
            error: function(xhr) {
                if (xhr.status === 400) {
                    alert(xhr.responseJSON.message);
                }
            }
        });
    });
});