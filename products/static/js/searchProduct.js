$(document).ready(function() {
    // Обработчик клика по кнопке поиска
    $('#searchButton').click(function() {
        const searchQuery = $('#searchInput').val().trim();
        let selectedLocations = $('#filterForm .form-check-input:checked').map(function() {
            return this.value;
        }).get().join(',');
        let priceMin = $('#priceMin').val();
        let priceMax = $('#priceMax').val();
        let quantityMin = $('#quantityMin').val();
        let quantityMax = $('#quantityMax').val();

        $.get('/products', {
            locations: selectedLocations,
            price_min: priceMin,
            price_max: priceMax,
            search_query: searchQuery,
            quantity_min: quantityMin,
            quantity_max: quantityMax
        }, function(data) {
            $('#tableContainer').html(data.table);
        });
    });

    // Обработчик нажатия клавиши в поле поиска
    $('#searchInput').keypress(function(e) {
        // Проверка на нажате Enter
        if (e.which === 13) {
            e.preventDefault();
            $('#searchButton').click();
        }
    });
});