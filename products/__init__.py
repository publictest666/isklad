import os
import json

from flask import Flask
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)

# Подключение к БД
with open(os.path.join(app.root_path, '..', 'db_settings.json'), 'r') as f:
    db_config = json.load(f)
db_uri = f"{db_config['DB_TYPE']}://{db_config['DB_USER']}:{db_config['DB_PASSWORD']}@{db_config['DB_HOST']}/{db_config['DB_NAME']}"
app.config['SQLALCHEMY_DATABASE_URI'] = db_uri
db = SQLAlchemy(app)

# Создание моделей
from products import models, routes

with app.app_context():
    db.create_all()
