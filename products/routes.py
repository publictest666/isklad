from flask import render_template, request, jsonify

from products import app, db
from products.models import Product, Location, Inventory


@app.route('/')
def index():
    return render_template('products/products.html', inventory_items=Inventory.query.all(),
                           locations=Location.query.all())


# Роут добавления товара на склад
@app.route('/add_to_quantity', methods=['POST'])
def add_to_quantity():
    inventory_id = request.json['inventory_id']
    quantity_change = request.json['quantity_change']

    inventory_item = Inventory.query.get(inventory_id)
    if inventory_item:
        if quantity_change > 0 or inventory_item.quantity > 0:
            inventory_item.quantity += quantity_change
            db.session.commit()
        return jsonify({'new_quantity': inventory_item.quantity})
    else:
        return jsonify({'message': 'Товар не найден'})


@app.route('/add_product', methods=['POST'])
def add_product():
    # Получаем данные из JSON тела запроса
    name = request.json['name']
    description = request.json['description']
    price = float(request.json['price'])
    location_id = int(request.json['location_id'])
    quantity = request.json['quantity']
    price_min = request.json['price_min']
    price_max = request.json['price_max']
    quantity_min = request.json['quantity_min']
    quantity_max = request.json['quantity_max']

    # Проверка на существование товара с таким именем
    existing_product = Product.query.filter_by(name=name).first()
    if existing_product:
        return jsonify({'message': 'Товар с таким именем уже существует'}), 400

    # Создание новой записи Товара
    new_product = Product(name=name, description=description, price=price)
    db.session.add(new_product)
    db.session.flush()

    # Создание новой записи Нахождения товара
    inventory_item = Inventory(product_id=new_product.id,
                               location_id=location_id,
                               quantity=quantity)
    db.session.add(inventory_item)
    db.session.commit()

    # Получаем строку поиска и выбранные локации для фильтрации нового значения, если они есть
    search_query = request.json.get('search_query', '').lower()
    selected_locations = request.json.get('selected_locations', [])

    # Проверяем, соответствует ли новый продукт текущим фильтрам
    should_render = (not search_query or search_query in name.lower()) and \
                    (not selected_locations or location_id in selected_locations) and \
                    (not price_min or price >= float(price_min)) and \
                    (not price_max or price <= float(price_max)) and \
                    (not quantity_min or quantity >= float(quantity_min)) and \
                    (not quantity_max or quantity <= float(quantity_max))

    # Используем новый файл шаблона для рендеринга строки, если необходимо
    rendered_row = render_template('products/product_row.html',
                                   inventory_item=inventory_item) if should_render else ''

    if not should_render:
        return jsonify({
            'rendered_row': rendered_row,
            'message': 'Товар создан, но не соответствует текущим фильтрациям'
        })

    return jsonify({
        'rendered_row': rendered_row,
    })


@app.route('/add_location', methods=['POST'])
def add_location():
    name = request.json['name']

    # Проверка на существование локации с таким именем
    existing_location = Location.query.filter_by(name=name).first()
    if existing_location:
        return jsonify({'message': 'Локация с таким именем уже существует'}), 400

    # Занесение новой Локации в БД
    new_location = Location(name=name)
    db.session.add(new_location)
    db.session.commit()

    return jsonify({
        'location': {
            'id': new_location.id,
            'name': new_location.name
        }
    })


@app.route('/products', methods=['GET', 'POST'])
def products():
    search_query = request.args.get('search_query')
    selected_locations = request.args.get('locations', '').split(',')
    selected_locations = [loc for loc in selected_locations if loc]
    price_min = request.args.get('price_min', type=float)
    price_max = request.args.get('price_max', type=float)
    quantity_min = request.args.get('quantity_min', type=int)
    quantity_max = request.args.get('quantity_max', type=int)

    query = Inventory.query.join(Product)
    # Фильтрация по названию
    if search_query:
        query = query.filter(Product.name.contains(search_query))
    # Фильтрация по локациям
    if selected_locations:
        query = query.filter(Inventory.location_id.in_(selected_locations))
    # Фильтрация по цене
    if price_min is not None:
        query = query.filter(Product.price >= price_min)
    if price_max is not None:
        query = query.filter(Product.price <= price_max)
    # Фильтрация по количеству товара
    if quantity_min is not None:
        query = query.filter(Inventory.quantity >= quantity_min)
    if quantity_max is not None:
        query = query.filter(Inventory.quantity <= quantity_max)

    inventory_items = query.all()

    # Проверяем, является ли запрос AJAX-запросом
    if request.headers.get('X-Requested-With') == 'XMLHttpRequest':
        rendered_table = render_template('products/products_table.html', inventory_items=inventory_items)
        return jsonify({'table': rendered_table})

    return render_template('products/products.html', inventory_items=inventory_items)
